// $ANTLR 3.4 /home/student/eclipse-workspace/antlr_swing/antlr_swing/src/tb/antlr/Expr.g 2020-04-01 03:51:58

package tb.antlr;


import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

import org.antlr.runtime.tree.*;


@SuppressWarnings({"all", "warnings", "unchecked"})
public class ExprParser extends Parser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "DIV", "ELSE", "EQUAL", "ID", "IF", "INT", "LP", "MINUS", "MUL", "NL", "NOTEQUAL", "PLUS", "PODST", "RP", "THEN", "VAR", "WS"
    };

    public static final int EOF=-1;
    public static final int DIV=4;
    public static final int ELSE=5;
    public static final int EQUAL=6;
    public static final int ID=7;
    public static final int IF=8;
    public static final int INT=9;
    public static final int LP=10;
    public static final int MINUS=11;
    public static final int MUL=12;
    public static final int NL=13;
    public static final int NOTEQUAL=14;
    public static final int PLUS=15;
    public static final int PODST=16;
    public static final int RP=17;
    public static final int THEN=18;
    public static final int VAR=19;
    public static final int WS=20;

    // delegates
    public Parser[] getDelegates() {
        return new Parser[] {};
    }

    // delegators


    public ExprParser(TokenStream input) {
        this(input, new RecognizerSharedState());
    }
    public ExprParser(TokenStream input, RecognizerSharedState state) {
        super(input, state);
    }

protected TreeAdaptor adaptor = new CommonTreeAdaptor();

public void setTreeAdaptor(TreeAdaptor adaptor) {
    this.adaptor = adaptor;
}
public TreeAdaptor getTreeAdaptor() {
    return adaptor;
}
    public String[] getTokenNames() { return ExprParser.tokenNames; }
    public String getGrammarFileName() { return "/home/student/eclipse-workspace/antlr_swing/antlr_swing/src/tb/antlr/Expr.g"; }


    public static class prog_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "prog"
    // /home/student/eclipse-workspace/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:16:1: prog : ( stat )+ EOF !;
    public final ExprParser.prog_return prog() throws RecognitionException {
        ExprParser.prog_return retval = new ExprParser.prog_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token EOF2=null;
        ExprParser.stat_return stat1 =null;


        CommonTree EOF2_tree=null;

        try {
            // /home/student/eclipse-workspace/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:17:5: ( ( stat )+ EOF !)
            // /home/student/eclipse-workspace/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:17:7: ( stat )+ EOF !
            {
            root_0 = (CommonTree)adaptor.nil();


            // /home/student/eclipse-workspace/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:17:7: ( stat )+
            int cnt1=0;
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( ((LA1_0 >= ID && LA1_0 <= LP)||LA1_0==NL||LA1_0==VAR) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // /home/student/eclipse-workspace/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:17:8: stat
            	    {
            	    pushFollow(FOLLOW_stat_in_prog49);
            	    stat1=stat();

            	    state._fsp--;

            	    adaptor.addChild(root_0, stat1.getTree());

            	    }
            	    break;

            	default :
            	    if ( cnt1 >= 1 ) break loop1;
                        EarlyExitException eee =
                            new EarlyExitException(1, input);
                        throw eee;
                }
                cnt1++;
            } while (true);


            EOF2=(Token)match(input,EOF,FOLLOW_EOF_in_prog54); 

            }

            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "prog"


    public static class stat_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "stat"
    // /home/student/eclipse-workspace/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:19:1: stat : ( expr NL -> expr | VAR ID NL -> ^( VAR ID ) | ID PODST expr NL -> ^( PODST ID expr ) | comp NL -> comp | NL ->);
    public final ExprParser.stat_return stat() throws RecognitionException {
        ExprParser.stat_return retval = new ExprParser.stat_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token NL4=null;
        Token VAR5=null;
        Token ID6=null;
        Token NL7=null;
        Token ID8=null;
        Token PODST9=null;
        Token NL11=null;
        Token NL13=null;
        Token NL14=null;
        ExprParser.expr_return expr3 =null;

        ExprParser.expr_return expr10 =null;

        ExprParser.comp_return comp12 =null;


        CommonTree NL4_tree=null;
        CommonTree VAR5_tree=null;
        CommonTree ID6_tree=null;
        CommonTree NL7_tree=null;
        CommonTree ID8_tree=null;
        CommonTree PODST9_tree=null;
        CommonTree NL11_tree=null;
        CommonTree NL13_tree=null;
        CommonTree NL14_tree=null;
        RewriteRuleTokenStream stream_VAR=new RewriteRuleTokenStream(adaptor,"token VAR");
        RewriteRuleTokenStream stream_PODST=new RewriteRuleTokenStream(adaptor,"token PODST");
        RewriteRuleTokenStream stream_ID=new RewriteRuleTokenStream(adaptor,"token ID");
        RewriteRuleTokenStream stream_NL=new RewriteRuleTokenStream(adaptor,"token NL");
        RewriteRuleSubtreeStream stream_comp=new RewriteRuleSubtreeStream(adaptor,"rule comp");
        RewriteRuleSubtreeStream stream_expr=new RewriteRuleSubtreeStream(adaptor,"rule expr");
        try {
            // /home/student/eclipse-workspace/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:20:5: ( expr NL -> expr | VAR ID NL -> ^( VAR ID ) | ID PODST expr NL -> ^( PODST ID expr ) | comp NL -> comp | NL ->)
            int alt2=5;
            switch ( input.LA(1) ) {
            case INT:
            case LP:
                {
                alt2=1;
                }
                break;
            case ID:
                {
                int LA2_2 = input.LA(2);

                if ( (LA2_2==PODST) ) {
                    alt2=3;
                }
                else if ( (LA2_2==DIV||LA2_2==EQUAL||(LA2_2 >= MINUS && LA2_2 <= PLUS)) ) {
                    alt2=1;
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 2, 2, input);

                    throw nvae;

                }
                }
                break;
            case VAR:
                {
                alt2=2;
                }
                break;
            case IF:
                {
                alt2=4;
                }
                break;
            case NL:
                {
                alt2=5;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;

            }

            switch (alt2) {
                case 1 :
                    // /home/student/eclipse-workspace/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:20:7: expr NL
                    {
                    pushFollow(FOLLOW_expr_in_stat67);
                    expr3=expr();

                    state._fsp--;

                    stream_expr.add(expr3.getTree());

                    NL4=(Token)match(input,NL,FOLLOW_NL_in_stat69);  
                    stream_NL.add(NL4);


                    // AST REWRITE
                    // elements: expr
                    // token labels: 
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

                    root_0 = (CommonTree)adaptor.nil();
                    // 20:15: -> expr
                    {
                        adaptor.addChild(root_0, stream_expr.nextTree());

                    }


                    retval.tree = root_0;

                    }
                    break;
                case 2 :
                    // /home/student/eclipse-workspace/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:23:7: VAR ID NL
                    {
                    VAR5=(Token)match(input,VAR,FOLLOW_VAR_in_stat83);  
                    stream_VAR.add(VAR5);


                    ID6=(Token)match(input,ID,FOLLOW_ID_in_stat85);  
                    stream_ID.add(ID6);


                    NL7=(Token)match(input,NL,FOLLOW_NL_in_stat87);  
                    stream_NL.add(NL7);


                    // AST REWRITE
                    // elements: ID, VAR
                    // token labels: 
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

                    root_0 = (CommonTree)adaptor.nil();
                    // 23:17: -> ^( VAR ID )
                    {
                        // /home/student/eclipse-workspace/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:23:20: ^( VAR ID )
                        {
                        CommonTree root_1 = (CommonTree)adaptor.nil();
                        root_1 = (CommonTree)adaptor.becomeRoot(
                        stream_VAR.nextNode()
                        , root_1);

                        adaptor.addChild(root_1, 
                        stream_ID.nextNode()
                        );

                        adaptor.addChild(root_0, root_1);
                        }

                    }


                    retval.tree = root_0;

                    }
                    break;
                case 3 :
                    // /home/student/eclipse-workspace/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:24:7: ID PODST expr NL
                    {
                    ID8=(Token)match(input,ID,FOLLOW_ID_in_stat103);  
                    stream_ID.add(ID8);


                    PODST9=(Token)match(input,PODST,FOLLOW_PODST_in_stat105);  
                    stream_PODST.add(PODST9);


                    pushFollow(FOLLOW_expr_in_stat107);
                    expr10=expr();

                    state._fsp--;

                    stream_expr.add(expr10.getTree());

                    NL11=(Token)match(input,NL,FOLLOW_NL_in_stat109);  
                    stream_NL.add(NL11);


                    // AST REWRITE
                    // elements: PODST, expr, ID
                    // token labels: 
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

                    root_0 = (CommonTree)adaptor.nil();
                    // 24:24: -> ^( PODST ID expr )
                    {
                        // /home/student/eclipse-workspace/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:24:27: ^( PODST ID expr )
                        {
                        CommonTree root_1 = (CommonTree)adaptor.nil();
                        root_1 = (CommonTree)adaptor.becomeRoot(
                        stream_PODST.nextNode()
                        , root_1);

                        adaptor.addChild(root_1, 
                        stream_ID.nextNode()
                        );

                        adaptor.addChild(root_1, stream_expr.nextTree());

                        adaptor.addChild(root_0, root_1);
                        }

                    }


                    retval.tree = root_0;

                    }
                    break;
                case 4 :
                    // /home/student/eclipse-workspace/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:25:7: comp NL
                    {
                    pushFollow(FOLLOW_comp_in_stat127);
                    comp12=comp();

                    state._fsp--;

                    stream_comp.add(comp12.getTree());

                    NL13=(Token)match(input,NL,FOLLOW_NL_in_stat129);  
                    stream_NL.add(NL13);


                    // AST REWRITE
                    // elements: comp
                    // token labels: 
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

                    root_0 = (CommonTree)adaptor.nil();
                    // 25:15: -> comp
                    {
                        adaptor.addChild(root_0, stream_comp.nextTree());

                    }


                    retval.tree = root_0;

                    }
                    break;
                case 5 :
                    // /home/student/eclipse-workspace/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:26:7: NL
                    {
                    NL14=(Token)match(input,NL,FOLLOW_NL_in_stat141);  
                    stream_NL.add(NL14);


                    // AST REWRITE
                    // elements: 
                    // token labels: 
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

                    root_0 = (CommonTree)adaptor.nil();
                    // 26:10: ->
                    {
                        root_0 = null;
                    }


                    retval.tree = root_0;

                    }
                    break;

            }
            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "stat"


    public static class comp_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "comp"
    // /home/student/eclipse-workspace/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:29:1: comp : IF ^i= expr THEN !j= expr ( ELSE !k= expr )? ;
    public final ExprParser.comp_return comp() throws RecognitionException {
        ExprParser.comp_return retval = new ExprParser.comp_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token IF15=null;
        Token THEN16=null;
        Token ELSE17=null;
        ExprParser.expr_return i =null;

        ExprParser.expr_return j =null;

        ExprParser.expr_return k =null;


        CommonTree IF15_tree=null;
        CommonTree THEN16_tree=null;
        CommonTree ELSE17_tree=null;

        try {
            // /home/student/eclipse-workspace/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:30:5: ( IF ^i= expr THEN !j= expr ( ELSE !k= expr )? )
            // /home/student/eclipse-workspace/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:30:7: IF ^i= expr THEN !j= expr ( ELSE !k= expr )?
            {
            root_0 = (CommonTree)adaptor.nil();


            IF15=(Token)match(input,IF,FOLLOW_IF_in_comp160); 
            IF15_tree = 
            (CommonTree)adaptor.create(IF15)
            ;
            root_0 = (CommonTree)adaptor.becomeRoot(IF15_tree, root_0);


            pushFollow(FOLLOW_expr_in_comp165);
            i=expr();

            state._fsp--;

            adaptor.addChild(root_0, i.getTree());

            THEN16=(Token)match(input,THEN,FOLLOW_THEN_in_comp167); 

            pushFollow(FOLLOW_expr_in_comp172);
            j=expr();

            state._fsp--;

            adaptor.addChild(root_0, j.getTree());

            // /home/student/eclipse-workspace/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:30:31: ( ELSE !k= expr )?
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0==ELSE) ) {
                alt3=1;
            }
            switch (alt3) {
                case 1 :
                    // /home/student/eclipse-workspace/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:30:32: ELSE !k= expr
                    {
                    ELSE17=(Token)match(input,ELSE,FOLLOW_ELSE_in_comp175); 

                    pushFollow(FOLLOW_expr_in_comp180);
                    k=expr();

                    state._fsp--;

                    adaptor.addChild(root_0, k.getTree());

                    }
                    break;

            }


            }

            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "comp"


    public static class expr_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "expr"
    // /home/student/eclipse-workspace/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:33:1: expr : addExpr ( EQUAL ^ addExpr | NOTEQUAL ^ addExpr )* ;
    public final ExprParser.expr_return expr() throws RecognitionException {
        ExprParser.expr_return retval = new ExprParser.expr_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token EQUAL19=null;
        Token NOTEQUAL21=null;
        ExprParser.addExpr_return addExpr18 =null;

        ExprParser.addExpr_return addExpr20 =null;

        ExprParser.addExpr_return addExpr22 =null;


        CommonTree EQUAL19_tree=null;
        CommonTree NOTEQUAL21_tree=null;

        try {
            // /home/student/eclipse-workspace/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:34:5: ( addExpr ( EQUAL ^ addExpr | NOTEQUAL ^ addExpr )* )
            // /home/student/eclipse-workspace/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:34:7: addExpr ( EQUAL ^ addExpr | NOTEQUAL ^ addExpr )*
            {
            root_0 = (CommonTree)adaptor.nil();


            pushFollow(FOLLOW_addExpr_in_expr199);
            addExpr18=addExpr();

            state._fsp--;

            adaptor.addChild(root_0, addExpr18.getTree());

            // /home/student/eclipse-workspace/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:35:7: ( EQUAL ^ addExpr | NOTEQUAL ^ addExpr )*
            loop4:
            do {
                int alt4=3;
                int LA4_0 = input.LA(1);

                if ( (LA4_0==EQUAL) ) {
                    alt4=1;
                }
                else if ( (LA4_0==NOTEQUAL) ) {
                    alt4=2;
                }


                switch (alt4) {
            	case 1 :
            	    // /home/student/eclipse-workspace/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:35:9: EQUAL ^ addExpr
            	    {
            	    EQUAL19=(Token)match(input,EQUAL,FOLLOW_EQUAL_in_expr209); 
            	    EQUAL19_tree = 
            	    (CommonTree)adaptor.create(EQUAL19)
            	    ;
            	    root_0 = (CommonTree)adaptor.becomeRoot(EQUAL19_tree, root_0);


            	    pushFollow(FOLLOW_addExpr_in_expr212);
            	    addExpr20=addExpr();

            	    state._fsp--;

            	    adaptor.addChild(root_0, addExpr20.getTree());

            	    }
            	    break;
            	case 2 :
            	    // /home/student/eclipse-workspace/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:36:9: NOTEQUAL ^ addExpr
            	    {
            	    NOTEQUAL21=(Token)match(input,NOTEQUAL,FOLLOW_NOTEQUAL_in_expr222); 
            	    NOTEQUAL21_tree = 
            	    (CommonTree)adaptor.create(NOTEQUAL21)
            	    ;
            	    root_0 = (CommonTree)adaptor.becomeRoot(NOTEQUAL21_tree, root_0);


            	    pushFollow(FOLLOW_addExpr_in_expr225);
            	    addExpr22=addExpr();

            	    state._fsp--;

            	    adaptor.addChild(root_0, addExpr22.getTree());

            	    }
            	    break;

            	default :
            	    break loop4;
                }
            } while (true);


            }

            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "expr"


    public static class addExpr_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "addExpr"
    // /home/student/eclipse-workspace/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:40:1: addExpr : multExpr ( PLUS ^ multExpr | MINUS ^ multExpr )* ;
    public final ExprParser.addExpr_return addExpr() throws RecognitionException {
        ExprParser.addExpr_return retval = new ExprParser.addExpr_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token PLUS24=null;
        Token MINUS26=null;
        ExprParser.multExpr_return multExpr23 =null;

        ExprParser.multExpr_return multExpr25 =null;

        ExprParser.multExpr_return multExpr27 =null;


        CommonTree PLUS24_tree=null;
        CommonTree MINUS26_tree=null;

        try {
            // /home/student/eclipse-workspace/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:41:5: ( multExpr ( PLUS ^ multExpr | MINUS ^ multExpr )* )
            // /home/student/eclipse-workspace/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:41:7: multExpr ( PLUS ^ multExpr | MINUS ^ multExpr )*
            {
            root_0 = (CommonTree)adaptor.nil();


            pushFollow(FOLLOW_multExpr_in_addExpr249);
            multExpr23=multExpr();

            state._fsp--;

            adaptor.addChild(root_0, multExpr23.getTree());

            // /home/student/eclipse-workspace/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:42:7: ( PLUS ^ multExpr | MINUS ^ multExpr )*
            loop5:
            do {
                int alt5=3;
                int LA5_0 = input.LA(1);

                if ( (LA5_0==PLUS) ) {
                    alt5=1;
                }
                else if ( (LA5_0==MINUS) ) {
                    alt5=2;
                }


                switch (alt5) {
            	case 1 :
            	    // /home/student/eclipse-workspace/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:42:9: PLUS ^ multExpr
            	    {
            	    PLUS24=(Token)match(input,PLUS,FOLLOW_PLUS_in_addExpr259); 
            	    PLUS24_tree = 
            	    (CommonTree)adaptor.create(PLUS24)
            	    ;
            	    root_0 = (CommonTree)adaptor.becomeRoot(PLUS24_tree, root_0);


            	    pushFollow(FOLLOW_multExpr_in_addExpr262);
            	    multExpr25=multExpr();

            	    state._fsp--;

            	    adaptor.addChild(root_0, multExpr25.getTree());

            	    }
            	    break;
            	case 2 :
            	    // /home/student/eclipse-workspace/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:43:9: MINUS ^ multExpr
            	    {
            	    MINUS26=(Token)match(input,MINUS,FOLLOW_MINUS_in_addExpr272); 
            	    MINUS26_tree = 
            	    (CommonTree)adaptor.create(MINUS26)
            	    ;
            	    root_0 = (CommonTree)adaptor.becomeRoot(MINUS26_tree, root_0);


            	    pushFollow(FOLLOW_multExpr_in_addExpr275);
            	    multExpr27=multExpr();

            	    state._fsp--;

            	    adaptor.addChild(root_0, multExpr27.getTree());

            	    }
            	    break;

            	default :
            	    break loop5;
                }
            } while (true);


            }

            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "addExpr"


    public static class multExpr_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "multExpr"
    // /home/student/eclipse-workspace/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:47:1: multExpr : atom ( MUL ^ atom | DIV ^ atom )* ;
    public final ExprParser.multExpr_return multExpr() throws RecognitionException {
        ExprParser.multExpr_return retval = new ExprParser.multExpr_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token MUL29=null;
        Token DIV31=null;
        ExprParser.atom_return atom28 =null;

        ExprParser.atom_return atom30 =null;

        ExprParser.atom_return atom32 =null;


        CommonTree MUL29_tree=null;
        CommonTree DIV31_tree=null;

        try {
            // /home/student/eclipse-workspace/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:48:5: ( atom ( MUL ^ atom | DIV ^ atom )* )
            // /home/student/eclipse-workspace/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:48:7: atom ( MUL ^ atom | DIV ^ atom )*
            {
            root_0 = (CommonTree)adaptor.nil();


            pushFollow(FOLLOW_atom_in_multExpr301);
            atom28=atom();

            state._fsp--;

            adaptor.addChild(root_0, atom28.getTree());

            // /home/student/eclipse-workspace/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:49:7: ( MUL ^ atom | DIV ^ atom )*
            loop6:
            do {
                int alt6=3;
                int LA6_0 = input.LA(1);

                if ( (LA6_0==MUL) ) {
                    alt6=1;
                }
                else if ( (LA6_0==DIV) ) {
                    alt6=2;
                }


                switch (alt6) {
            	case 1 :
            	    // /home/student/eclipse-workspace/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:49:9: MUL ^ atom
            	    {
            	    MUL29=(Token)match(input,MUL,FOLLOW_MUL_in_multExpr311); 
            	    MUL29_tree = 
            	    (CommonTree)adaptor.create(MUL29)
            	    ;
            	    root_0 = (CommonTree)adaptor.becomeRoot(MUL29_tree, root_0);


            	    pushFollow(FOLLOW_atom_in_multExpr314);
            	    atom30=atom();

            	    state._fsp--;

            	    adaptor.addChild(root_0, atom30.getTree());

            	    }
            	    break;
            	case 2 :
            	    // /home/student/eclipse-workspace/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:50:9: DIV ^ atom
            	    {
            	    DIV31=(Token)match(input,DIV,FOLLOW_DIV_in_multExpr324); 
            	    DIV31_tree = 
            	    (CommonTree)adaptor.create(DIV31)
            	    ;
            	    root_0 = (CommonTree)adaptor.becomeRoot(DIV31_tree, root_0);


            	    pushFollow(FOLLOW_atom_in_multExpr327);
            	    atom32=atom();

            	    state._fsp--;

            	    adaptor.addChild(root_0, atom32.getTree());

            	    }
            	    break;

            	default :
            	    break loop6;
                }
            } while (true);


            }

            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "multExpr"


    public static class atom_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "atom"
    // /home/student/eclipse-workspace/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:54:1: atom : ( INT | ID | LP ! expr RP !);
    public final ExprParser.atom_return atom() throws RecognitionException {
        ExprParser.atom_return retval = new ExprParser.atom_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token INT33=null;
        Token ID34=null;
        Token LP35=null;
        Token RP37=null;
        ExprParser.expr_return expr36 =null;


        CommonTree INT33_tree=null;
        CommonTree ID34_tree=null;
        CommonTree LP35_tree=null;
        CommonTree RP37_tree=null;

        try {
            // /home/student/eclipse-workspace/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:55:5: ( INT | ID | LP ! expr RP !)
            int alt7=3;
            switch ( input.LA(1) ) {
            case INT:
                {
                alt7=1;
                }
                break;
            case ID:
                {
                alt7=2;
                }
                break;
            case LP:
                {
                alt7=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 7, 0, input);

                throw nvae;

            }

            switch (alt7) {
                case 1 :
                    // /home/student/eclipse-workspace/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:55:9: INT
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    INT33=(Token)match(input,INT,FOLLOW_INT_in_atom355); 
                    INT33_tree = 
                    (CommonTree)adaptor.create(INT33)
                    ;
                    adaptor.addChild(root_0, INT33_tree);


                    }
                    break;
                case 2 :
                    // /home/student/eclipse-workspace/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:56:9: ID
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    ID34=(Token)match(input,ID,FOLLOW_ID_in_atom365); 
                    ID34_tree = 
                    (CommonTree)adaptor.create(ID34)
                    ;
                    adaptor.addChild(root_0, ID34_tree);


                    }
                    break;
                case 3 :
                    // /home/student/eclipse-workspace/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:57:9: LP ! expr RP !
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    LP35=(Token)match(input,LP,FOLLOW_LP_in_atom375); 

                    pushFollow(FOLLOW_expr_in_atom378);
                    expr36=expr();

                    state._fsp--;

                    adaptor.addChild(root_0, expr36.getTree());

                    RP37=(Token)match(input,RP,FOLLOW_RP_in_atom380); 

                    }
                    break;

            }
            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "atom"

    // Delegated rules


 

    public static final BitSet FOLLOW_stat_in_prog49 = new BitSet(new long[]{0x0000000000082780L});
    public static final BitSet FOLLOW_EOF_in_prog54 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_expr_in_stat67 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_NL_in_stat69 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_VAR_in_stat83 = new BitSet(new long[]{0x0000000000000080L});
    public static final BitSet FOLLOW_ID_in_stat85 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_NL_in_stat87 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ID_in_stat103 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_PODST_in_stat105 = new BitSet(new long[]{0x0000000000000680L});
    public static final BitSet FOLLOW_expr_in_stat107 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_NL_in_stat109 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_comp_in_stat127 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_NL_in_stat129 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_NL_in_stat141 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_IF_in_comp160 = new BitSet(new long[]{0x0000000000000680L});
    public static final BitSet FOLLOW_expr_in_comp165 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_THEN_in_comp167 = new BitSet(new long[]{0x0000000000000680L});
    public static final BitSet FOLLOW_expr_in_comp172 = new BitSet(new long[]{0x0000000000000022L});
    public static final BitSet FOLLOW_ELSE_in_comp175 = new BitSet(new long[]{0x0000000000000680L});
    public static final BitSet FOLLOW_expr_in_comp180 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_addExpr_in_expr199 = new BitSet(new long[]{0x0000000000004042L});
    public static final BitSet FOLLOW_EQUAL_in_expr209 = new BitSet(new long[]{0x0000000000000680L});
    public static final BitSet FOLLOW_addExpr_in_expr212 = new BitSet(new long[]{0x0000000000004042L});
    public static final BitSet FOLLOW_NOTEQUAL_in_expr222 = new BitSet(new long[]{0x0000000000000680L});
    public static final BitSet FOLLOW_addExpr_in_expr225 = new BitSet(new long[]{0x0000000000004042L});
    public static final BitSet FOLLOW_multExpr_in_addExpr249 = new BitSet(new long[]{0x0000000000008802L});
    public static final BitSet FOLLOW_PLUS_in_addExpr259 = new BitSet(new long[]{0x0000000000000680L});
    public static final BitSet FOLLOW_multExpr_in_addExpr262 = new BitSet(new long[]{0x0000000000008802L});
    public static final BitSet FOLLOW_MINUS_in_addExpr272 = new BitSet(new long[]{0x0000000000000680L});
    public static final BitSet FOLLOW_multExpr_in_addExpr275 = new BitSet(new long[]{0x0000000000008802L});
    public static final BitSet FOLLOW_atom_in_multExpr301 = new BitSet(new long[]{0x0000000000001012L});
    public static final BitSet FOLLOW_MUL_in_multExpr311 = new BitSet(new long[]{0x0000000000000680L});
    public static final BitSet FOLLOW_atom_in_multExpr314 = new BitSet(new long[]{0x0000000000001012L});
    public static final BitSet FOLLOW_DIV_in_multExpr324 = new BitSet(new long[]{0x0000000000000680L});
    public static final BitSet FOLLOW_atom_in_multExpr327 = new BitSet(new long[]{0x0000000000001012L});
    public static final BitSet FOLLOW_INT_in_atom355 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ID_in_atom365 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_LP_in_atom375 = new BitSet(new long[]{0x0000000000000680L});
    public static final BitSet FOLLOW_expr_in_atom378 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_RP_in_atom380 = new BitSet(new long[]{0x0000000000000002L});

}